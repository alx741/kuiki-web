{-# LANGUAGE GADTs             #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import Web.Spock

import App
import Models

main :: IO ()
main = do
    dbPool <- createConnectionPoolDB
    cfg <- spockCfg dbPool
    migrateDB dbPool
    runSpock 3000 $ spock cfg app
