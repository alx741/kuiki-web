{-# LANGUAGE GADTs             #-}
{-# LANGUAGE OverloadedStrings #-}

module App where

import Control.Monad.Logger
import Data.Pool                       (Pool)
import Database.Persist.Postgresql     (ConnectionString, SqlBackend,
                                        createPostgresqlPool)
import Network.Wai.Middleware.Static
import Web.Spock
import Web.Spock.Config
import Web.Spock.Internal.SessionVault (newStmSessionStore)

import Handlers
import Routes

dbConnStr :: ConnectionString
dbConnStr = "host=localhost dbname=kuiki user=alx password=0x44616e696c6f port=5432"

sessionCfg :: IO (SessionCfg conn () st)
sessionCfg = do
    store <- newStmSessionStore
    return SessionCfg
        { sc_cookieName = "kuiki_cookie"
        , sc_sessionTTL = 3600
        , sc_sessionIdEntropy = 64
        , sc_sessionExpandTTL = True
        , sc_emptySession = ()
        , sc_store = store
        , sc_housekeepingInterval = 60 * 10
        , sc_hooks = defaultSessionHooks
        }

spockCfg :: Pool SqlBackend -> IO (SpockCfg SqlBackend () ())
spockCfg dbPool = do
    sess <- sessionCfg
    return SpockCfg
        { spc_initialState = ()
        , spc_database = PCPool dbPool
        , spc_sessionCfg = sess
        , spc_maxRequestSize = Just (5 * 1024 * 1024)
        , spc_errorHandler = errorHandler
        , spc_csrfProtection = True
        , spc_csrfHeaderName = "X-Csrf-Token"
        , spc_csrfPostName = "__csrf_token"
        }

createConnectionPoolDB :: IO (Pool SqlBackend)
createConnectionPoolDB = runStderrLoggingT $ do
    createPostgresqlPool dbConnStr 10


app :: SpockM SqlBackend () () ()
app = do
    middleware $ staticPolicy $ addBase "static"
    middleware $ staticPolicy $ addBase "/opt/kuiki/static"

    get root          getRootHandler
    get destinationsR getDestinationsHandler
    get mainR         getMainHandler
    get applyR        getApplyHandler
    post applyR        postApplyHandler

    -- Destinations
    get destCuencaR   getDestCuencaHandler
