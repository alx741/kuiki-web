{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wno-missing-signatures #-}

module Routes where

import Web.Spock

-- | --------------------------------------------
-- |  Routes
-- | --------------------------------------------

destinationsR = static "destinations"
mainR = static "main"
applyR = static "apply"

-- Destinations
destCuencaR = static "destinations" <//> "cuenca"
