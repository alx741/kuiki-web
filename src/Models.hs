{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

{-# OPTIONS_GHC -Wno-missing-signatures  #-}

module Models where

import Data.Pool            (Pool)
import Database.Persist.Sql
import Database.Persist.TH
import Web.Spock

-- | --------------------------------------------
-- |  Models
-- | --------------------------------------------


share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|

User
    name String
    deriving Show

|]


migrateDB :: Pool SqlBackend -> IO ()
migrateDB dbPool = runSqlPool (runMigration migrateAll) dbPool

-- Helper function for running persistent queries
runDB query = runQuery $ \dbConn -> flip runSqlPersistM dbConn $ query
