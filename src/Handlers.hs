{-# LANGUAGE GADTs             #-}
{-# LANGUAGE OverloadedStrings #-}

module Handlers where

import Control.Monad.IO.Class    (liftIO)
import Data.Maybe (isJust)
import Data.Monoid               ((<>))
import Data.Text
import Database.Persist.Sql
import Network.HTTP.Types.Status (Status (..))
import Text.Mustache             (ToMustache (..), automaticCompile, object,
                                  substitute, checkedSubstitute, (~>))
import Text.Parsec.Error
import Web.Spock hiding (text)
import Text.Digestive
import Web.Spock.Digestive

import Models

type Handler = SpockAction SqlBackend () ()


data Person = Person
    { age  :: Int
    , name :: Text
    } deriving (Show)

instance ToMustache Person where
    toMustache person = object
        [ "age" ~> age person
        , "name" ~> name person
        ]

templatesDirs :: [FilePath]
templatesDirs = ["templates", "/opt/kuiki/templates"]

template :: String -> Handler ()
template template = do
    res <- liftIO $ loadTemplate template
    case res of
        Left _     -> error "oops"
        Right res' -> html res'

loadTemplate :: String -> IO (Either ParseError Text)
loadTemplate template = do
    let somePerson = Person 23 "Daniel"
    compiled <- automaticCompile templatesDirs $ template <> ".mustache"
    case compiled of
        Left err       -> pure $ Left err
        Right template -> pure $ Right $ substitute template somePerson


-- | --------------------------------------------
-- |  Handlers
-- | --------------------------------------------

getRootHandler :: Handler ()
getRootHandler = template "home"

getDestCuencaHandler :: Handler ()
getDestCuencaHandler = template "destinations/cuenca"

getDestinationsHandler :: Handler ()
getDestinationsHandler = template "destinations"

getMainHandler :: Handler ()
getMainHandler = do
    runDB $ insert $ User "daniel"
    template "main"

data UserF = UserF
        { userName :: Text
        , userMail :: Text
        } deriving (Show)

-- userForm :: Form Text IO UserF
userForm = UserF
    <$> "name" .: text Nothing
    <*> "mail" .: check "Email invalido" checkEmail (text Nothing)

checkEmail :: Text -> Bool
checkEmail = isJust . find (== '@')

getApplyHandler :: Handler ()
getApplyHandler = do
    csrfToken <- getCsrfToken
    liftIO $ print csrfToken
    compiled <- liftIO $ automaticCompile templatesDirs $ "apply" <> ".mustache"
    case compiled of
        Left err       -> error "oops"
        -- Right template -> html $ snd $ checkedSubstitute template csrfToken
        Right template -> do
            -- liftIO $ print "hola"
            let (errors, res) = checkedSubstitute template csrfToken
            liftIO $ print $ Prelude.head errors
            html res
    -- template "apply"

postApplyHandler :: Handler ()
postApplyHandler = do
    (v, mUser)  <- runForm "userForm" userForm
    liftIO $ print mUser
    template "apply"

errorHandler :: Status -> ActionCtxT () IO ()
errorHandler = error "Show some nice error pages :)"
