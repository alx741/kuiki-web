#!/bin/sh

APP_NAME="kuiki"
IMAGE_NAME="kuiki-kuiki-exe"
CONTAINER_NAME="kuiki"

DEPLOY_USER="alx"
DEPLOY_HOST="kuikitrip.com"
DEPLOY_PATH="/opt/kuiki"

APP_PORT=3000
EXPOSED_PORT=3000


echo
echo "[!] Saving docker image"
docker save "$IMAGE_NAME" -o "$IMAGE_NAME.dockerimage.tar"

echo
echo "[!] Compressing docker image"
gzip -f "$IMAGE_NAME.dockerimage.tar"

imagesize=$(du -h "$IMAGE_NAME.dockerimage.tar.gz")

echo
echo "[!] Uploading docker image ($imagesize)"
scp "$IMAGE_NAME.dockerimage.tar.gz" "$DEPLOY_USER@$DEPLOY_HOST:$DEPLOY_PATH/$IMAGE_NAME.dockerimage.tar.gz"

echo
echo "[!] Creating new container"

# read -s -p "PGPASS? " pgpass

ssh "$DEPLOY_USER@$DEPLOY_HOST" << EOF
    gzip -df "$DEPLOY_PATH/$IMAGE_NAME.dockerimage.tar.gz"
    sudo docker stop "$CONTAINER_NAME"
    sudo docker rm -f "$CONTAINER_NAME"
    sudo docker load -i "$DEPLOY_PATH/$IMAGE_NAME.dockerimage.tar"
    sudo docker run -d \
        --net="host" \
        --restart always \
        -p "$EXPOSED_PORT:$APP_PORT" \
        -v "/opt/$APP_NAME/etc:/opt/$APP_NAME/etc" \
        --name "$CONTAINER_NAME" \
        "$IMAGE_NAME"
EOF
