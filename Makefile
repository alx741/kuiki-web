# Frontend compilation
.PHONY: sass
sass:
	@npm run watch

.PHONY: js
js:
	@npm run webpack


# Development server
devel:
	@ghcid --command "stack ghci kuiki" --warnings --test "main"


# Sass compilation
SASS_DIR = ./sass
CSS_DIR = ./static/css
SASS_FILES = $(foreach input, $(shell find $(SASS_DIR) -type f -name '*.sass'), $(notdir $(input)))
CSS_FILES = $(foreach input, $(SASS_FILES), $(notdir $(input:.sass=.css)))
SASS_INPUTS = $(foreach input, $(SASS_FILES), $(addprefix $(SASS_DIR)/, $(input)))
CSS_OUTPUTS = $(foreach input, $(CSS_FILES), $(addprefix $(CSS_DIR)/, $(input)))

sass: $(CSS_OUTPUTS)

$(CSS_DIR)/%.css: $(SASS_DIR)/%.sass
	@sassc -t compressed $< $@

# Deployment
docker-image:
	stack build --docker && stack image container --docker

deploy: $(CSS_OUTPUTS) docker-image
	@./deploy.sh
